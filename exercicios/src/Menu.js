import React from 'react'
// import { createDrawerNavigator } from 'react-navigation'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'

import Simples from './components/Simples'
import ParImpar from './components/ParImpar'
import Inverter, { MegaSena } from './components/Multi'
import Contador from './components/Contador';
import Plataformas from './components/Plataformas';
import ValidarProps from './components/ValidarProps';
import Evento from './components/Evento';
import Avo from './components/ComunicacaoDireta';
import TextoSicronizado from './components/ComunicacaoIndireta';
import ListaFlex from './components/ListaFlex';
import Flex from './components/Flex';

// export default createDrawerNavigator({
//     MegaSena: {
//         screen: () => <MegaSena numeros={8} />,
//         navigationOptions: { title: 'Mega Sena' }
//     },
//     Inverter: {
//         screen: () => <Inverter texto='React Nativo!' />
//     },
//     ParImpar: {
//         screen: () => <ParImpar numero={30} />,
//         navigationOptions: { title: 'Par & Ímpar' }
//     },
//     Simples: {
//         screen: () => <Simples texto='Flexível!!!' />
//     }
// }, { drawerWidth: 300 })




export default function Menu(){
    
    const Stack = createStackNavigator();
    
    return <NavigationContainer>
                <Stack.Navigator >
                    <Stack.Screen name="Flex"  >
                        {props => <Flex/>}
                    </Stack.Screen>
                    <Stack.Screen name="Lista (Flex Box)"  >
                        {props => <ListaFlex/>}
                    </Stack.Screen>
                    <Stack.Screen name="Comunicação Indireta"  >
                        {props => <TextoSicronizado/>}
                    </Stack.Screen>
                    <Stack.Screen name="Comunicação Direta"  >
                        {props => <Avo nome="João" sobrenome="Silva" />}
                    </Stack.Screen>
                    <Stack.Screen name="Evento"  >
                        {props => <Evento />}
                    </Stack.Screen>

                    <Stack.Screen name="Validar Props"  >
                        {props => <ValidarProps ano={18} labe="Teste" />}
                    </Stack.Screen>    
                    <Stack.Screen name="Plataformas"  >
                        {props => <Plataformas />}
                    </Stack.Screen>
                    <Stack.Screen name="Contador"  >
                        {props => <Contador  numeroInicial={0} />}
                    </Stack.Screen>

                    <Stack.Screen name="Mega Sena"  >
                        {props => <MegaSena  numeros={8} />}
                    </Stack.Screen>
                    <Stack.Screen name="Inverte">
                        {props=> <Inverter texto='React Nativo!'/>}
                    </Stack.Screen>    
                    <Stack.Screen name="Par ou Impar">
                        {props=> <ParImpar numero={30}/>}
                    </Stack.Screen>
                    <Stack.Screen name="Simples">
                        {props=> <Simples texto='Flexível!!!' />}
                    </Stack.Screen>    
                </Stack.Navigator> 
        
        </NavigationContainer>
}